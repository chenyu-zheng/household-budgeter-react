import { apiUrl, apiHeaders } from './config.json';
import { send, createAuthHeaders, formatUrl } from './request';

const endpoints = {
  get: 'Invitations/{uid}',
  create: 'Invitations',
  delete: 'Invitations/{uid}',
  accept: 'Invitations/{uid}/acceptance',
}

export async function get(token, uid) {
  const response = await send(apiUrl + formatUrl(endpoints.get, {uid}), {
    method: 'GET',
    headers: createAuthHeaders(token, apiHeaders)
  })
  return response;
}

export async function create(token, householdUid, inviteeEmail) {
  const response = await send(apiUrl + endpoints.create, {
    method: 'POST',
    headers: createAuthHeaders(token, apiHeaders),
    body: JSON.stringify({
      householdUid,
      inviteeEmail
    })
  })
  return response;
}

export async function remove(token, uid) {
  const response = await send(apiUrl + formatUrl(endpoints.delete, {uid}), {
    method: 'DELETE',
    headers: createAuthHeaders(token, apiHeaders)
  })
  return response;
}

export async function accept(token, uid) {
  const response = await send(apiUrl + formatUrl(endpoints.accept, {uid}), {
    method: 'POST',
    headers: createAuthHeaders(token, apiHeaders)
  })
  return response;
}