import { tokenUrl } from './config.json';
import { send } from './request';

export async function getToken(username, password){
  const response = await send(tokenUrl, {
    method: 'POST',
    body: `username=${username}&password=${password}&grant_type=password`
  })
  return response;
}