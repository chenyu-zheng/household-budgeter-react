import { apiUrl, apiHeaders } from './config.json';
import { send, createAuthHeaders, formatUrl } from './request';

const endpoints = {
  get: 'Transactions/{uid}',
  create: 'Transactions',
  update: 'Transactions/{uid}',
  void: 'Transactions/{uid}/void'
}

export async function get(token, uid) {
  const response = await send(apiUrl + formatUrl(endpoints.get, {uid}), {
    method: 'GET',
    headers: createAuthHeaders(token, apiHeaders)
  })
  return response;
}

export async function create(token, amount, description, householdUid, hAccountUid, categoryUid) {
  const response = await send(apiUrl + endpoints.create, {
    method: 'POST',
    headers: createAuthHeaders(token, apiHeaders),
    body: JSON.stringify({
      amount,
      description,
      householdUid,
      hAccountUid,
      categoryUid
    })
  })
  return response;
}

export async function update(token, uid, description, categoryUid) {
  const response = await send(apiUrl + formatUrl(endpoints.update, {uid}), {
    method: 'PUT',
    headers: createAuthHeaders(token, apiHeaders),
    body: JSON.stringify({
      description,
      categoryUid
    })
  })
  return response;
}

export async function revoke(token, uid) {
  const response = await send(apiUrl + formatUrl(endpoints.void, {uid}), {
    method: 'POST',
    headers: createAuthHeaders(token, apiHeaders)
  })
  return response;
}