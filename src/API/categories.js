import { apiUrl, apiHeaders } from './config.json';
import { send, createAuthHeaders, formatUrl } from './request';

const endpoints = {
  get: 'Categories/{uid}',
  create: 'Categories',
  update: 'Categories/{uid}',
  delete: 'Categories/{uid}',
  transactions: 'Categories/{uid}/transactions',
}

export async function get(token, uid) {
  const response = await send(apiUrl + formatUrl(endpoints.get, {uid}), {
    method: 'GET',
    headers: createAuthHeaders(token, apiHeaders)
  })
  return response;
}

export async function create(token, householdUid, name) {
  const response = await send(apiUrl + endpoints.create, {
    method: 'POST',
    headers: createAuthHeaders(token, apiHeaders),
    body: JSON.stringify({
      householdUid,
      name
    })
  })
  return response;
}

export async function update(token, uid, name) {
  const response = await send(apiUrl + formatUrl(endpoints.update, {uid}), {
    method: 'PUT',
    headers: createAuthHeaders(token, apiHeaders),
    body: JSON.stringify({
      name
    })
  })
  return response;
}

export async function remove(token, uid) {
  const response = await send(apiUrl + formatUrl(endpoints.delete, {uid}), {
    method: 'DELETE',
    headers: createAuthHeaders(token, apiHeaders)
  })
  return response;
}

export async function listTransactions(token, uid) {
  const response = await send(apiUrl + formatUrl(endpoints.transactions, {uid}), {
    method: 'GET',
    headers: createAuthHeaders(token, apiHeaders)
  })
  return response;
}