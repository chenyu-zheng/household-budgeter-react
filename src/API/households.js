import { apiUrl, apiHeaders } from './config.json';
import { send, createAuthHeaders, formatUrl } from './request';

const endpoints = {
  get: 'Households/{uid}',
  create: 'Households',
  update: 'Households/{uid}',
  remove: 'Households/{uid}',
  leave: 'Households/{uid}/leave',
  listMembers: 'Households/{uid}/members',
  removeMember: 'Households/{uid}/removeMember',
  listInvitations: 'Households/{uid}/invitations',
  listCategories: 'Households/{uid}/categories',
  listHAccounts: 'Households/{uid}/haccounts',
}

export async function get(token, uid) {
  const response = await send(apiUrl + formatUrl(endpoints.get, {uid}), {
    method: 'GET',
    headers: createAuthHeaders(token, apiHeaders)
  })
  return response;
}

export async function create(token, name) {
  const response = await send(apiUrl + endpoints.create, {
    method: 'POST',
    headers: createAuthHeaders(token, apiHeaders),
    body: JSON.stringify({
      name
    })
  })
  return response;
}

export async function update(token, uid, name) {
  const response = await send(apiUrl + formatUrl(endpoints.update, {uid}), {
    method: 'PUT',
    headers: createAuthHeaders(token, apiHeaders),
    body: JSON.stringify({
      name
    })
  })
  return response;
}

export async function remove(token, uid) {
  const response = await send(apiUrl + formatUrl(endpoints.remove, {uid}), {
    method: 'DELETE',
    headers: createAuthHeaders(token, apiHeaders)
  })
  return response;
}

export async function leave(token, uid) {
  const response = await send(apiUrl + formatUrl(endpoints.leave, {uid}), {
    method: 'DELETE',
    headers: createAuthHeaders(token, apiHeaders)
  })
  return response;
}

export async function listMembers(token, uid) {
  const response = await send(apiUrl + formatUrl(endpoints.listMembers, {uid}), {
    method: 'GET',
    headers: createAuthHeaders(token, apiHeaders)
  })
  return response;
}

export async function removeMember(token, uid, memberId) {
  const response = await send(apiUrl + formatUrl(endpoints.removeMember, {uid}), {
    method: 'DELETE',
    headers: createAuthHeaders(token, apiHeaders),
    body: JSON.stringify({
      memberId
    })
  })
  return response;
}

export async function listInvitations(token, uid) {
  const response = await send(apiUrl + formatUrl(endpoints.listInvitations, {uid}), {
    method: 'GET',
    headers: createAuthHeaders(token, apiHeaders)
  })
  return response;
}

export async function listCategories(token, uid) {
  const response = await send(apiUrl + formatUrl(endpoints.listCategories, {uid}), {
    method: 'GET',
    headers: createAuthHeaders(token, apiHeaders)
  })
  return response;
}

export async function listHAccounts(token, uid) {
  const response = await send(apiUrl + formatUrl(endpoints.listHAccounts, {uid}), {
    method: 'GET',
    headers: createAuthHeaders(token, apiHeaders)
  })
  return response;
}