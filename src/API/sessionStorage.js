const storage = window.sessionStorage;

export function save(key, value) {
  storage.setItem(key, value);
}

export function read(key) {
  return storage.getItem(key);
}

export function clear() {
  storage.clear();
}