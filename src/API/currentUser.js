import { apiUrl, apiHeaders } from './config.json';
import { send, createAuthHeaders } from './request';

const endpoints = {
  myInvitations: 'me/invitations',
  myHouseholds: 'me/households/joined'
}

export async function listInvitations(token) {
  const response = await send(apiUrl + endpoints.myInvitations, {
    method: 'GET',
    headers: createAuthHeaders(token, apiHeaders)
  })
  return response;
}

export async function listHouseholds(token) {
  const response = await send(apiUrl + endpoints.myHouseholds, {
    method: 'GET',
    headers: createAuthHeaders(token, apiHeaders)
  })
  return response;
}