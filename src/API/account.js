import { apiUrl, apiHeaders } from './config.json';
import { send, createAuthHeaders } from './request';

const endpoints = {
  register: 'Account/Register',
  changePassword: 'account/changePassword',
  forgotPassword: 'account/forgotPassword',
  resetPassword: 'account/resetPassword'
}

export async function register(email, password, confirmPassword, firstName, lastName) {
  const response = await send(apiUrl + endpoints.register, {
    method: 'POST',
    headers: apiHeaders,
    body: JSON.stringify({
      email,
      password,
      confirmPassword,
      firstName,
      lastName
    })
  })
  return response;
}

export async function changePassword(token, oldPassword, newPassword, confirmPassword) {
  const response = await send(apiUrl + endpoints.changePassword, {
    method: 'POST',
    headers: createAuthHeaders(token, apiHeaders),
    body: JSON.stringify({
      oldPassword,
      newPassword,
      confirmPassword
    })
  })
  return response;
}

export async function forgotPassword(email) {
  const response = await send(apiUrl + endpoints.forgotPassword, {
    method: 'POST',
    headers: apiHeaders,
    body: JSON.stringify({
      email
    })
  })
  return response;
}

export async function resetPassword(email, password, confirmPassword, code) {
  const response = await send(apiUrl + endpoints.resetPassword, {
    method: 'POST',
    headers: apiHeaders,
    body: JSON.stringify({
      email,
      password,
      confirmPassword,
      code
    })
  })
  return response;
}