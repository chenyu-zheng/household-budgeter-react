export async function send(url, { method = "GET", headers = {}, body = null } = {}) {
  try {
    // console.log(`Send Request\nUrl: ${url}`);
    // console.log('Headers:', headers);
    // console.log('Body:', body);
    const response = await fetch(url, { method, headers, body });
    const result = {
      ok: response.ok,
      status: response.status,
      statusText: response.statusText,
      data: null
    };
    try {
      result.data = await response.json();
    } catch {
      result.data = {};
    }
    // console.log('Response:', result);
    if (!response.ok) {
      result.data = formErrorList(result.data);
    }
    return result;
  } catch (err) {
    console.error(err);
  }
}

export function createAuthHeaders(token, headers = {}) {
  return Object.assign({
    'Authorization': 'Bearer ' + token
  }, headers);
}

export function formatUrl(urlStr, params) {
  let url = urlStr;
  Object.keys(params).forEach(key => url = url.replace(`{${key}}`, params[key]));
  return url;
}

export function formErrorList(data) {
  let errors = [];
  try {
    if (data['modelState']) {
      const obj = data['modelState'];
      const keys = Object.keys(obj);
      errors = keys.map(key => obj[key]);
    } else if (data['error_description']) {
      errors.push(data['error_description'])
    } else if (data['error']) {
      errors.push(data['error'])
    } else if (data['message']) {
      errors.push(data['message'])
    } else {
      errors.push('Unknown error.');
      console.error(`An unknown error has occurred. Response body:\n${JSON.stringify(data)}`);
    }
  } catch (err) {
    console.error(err);
  }
  return errors;
}