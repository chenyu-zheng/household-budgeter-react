import React, { Component } from 'react';
import { Route, withRouter, Switch } from 'react-router-dom';
import './App.css';
import TopNavbar from './components/TopNavbar';
import Login from './scenes/Login';
import Register from './scenes/account/Register';
import CreateHousehold from './scenes/households/CreateHousehold';
import HouseholdList from './scenes/households/HouseholdList';
import AuthRoute from './components/AuthRoute';
import Manage from './scenes/Manage/Manage';
import ChangePassword from './scenes/Manage/ChangePassword';
import ForgotPassword from './scenes/account/ForgotPassword';
import ResetPassword from './scenes/account/ResetPassword';
import HouseholdDetails from './scenes/households/HouseholdDetails';
import UserInvites from './scenes/Invites/UserInvites';
import Home from './scenes/Home';
import Transactions from './scenes/transactions/Transactions';
import CreateTransaction from './scenes/transactions/CreateTransaction';
import NotFound from './scenes/NotFound';

// @inject('rootStore')
// @observer
class App extends Component {
  state = {
    username: null
  };

  render() {
    return (
      <div className="app">

        <TopNavbar username={this.state.username} />
    
        <Switch>
          <Route exact path="/" component={Home} />

          <Route path="/login" component={Login} />

          <Route path="/account/register" component={Register} />
          <Route path="/account/forgotPassword" component={ForgotPassword} />
          <Route path="/account/resetPassword" component={ResetPassword} />

          <AuthRoute exact path="/manage" component={Manage} />
          <AuthRoute path="/manage/changePassword" component={ChangePassword} />

          <AuthRoute path="/invites" component={UserInvites} />

          <AuthRoute exact path="/households" component={HouseholdList} />
          <AuthRoute
            path="/households/create"
            render={() => <CreateHousehold returnUrl="/households" />}
          />
          <AuthRoute exact path="/households/:uid" component={HouseholdDetails} />

          <AuthRoute exact path="/households/:uid/transactions" component={Transactions} />
          <AuthRoute exact path="/households/:uid/transactions/create" component={CreateTransaction} />
          <AuthRoute exact path="/households/:uid/transactions/:tUid/Edit" component={CreateTransaction} />

          <Route component={NotFound} />
        </Switch>
      </div>
    );
  }
}

export default withRouter(App);
