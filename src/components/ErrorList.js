import React, { Component } from 'react';
import { Alert } from 'react-bootstrap/lib';

class ErrorList extends Component {
  render() {
    return (
      <div>
        {this.props.errors.map((error, i) => (
          <Alert bsStyle="danger" key={i.toString()}>{error}</Alert>
        ))}
      </div>
    );
  }
}

export default ErrorList;
