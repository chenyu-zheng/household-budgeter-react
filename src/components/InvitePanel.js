import React from 'react';
import { Panel, Form, FormGroup, Col, FormControl, Button } from 'react-bootstrap/lib';
import { StoreInputHandler } from './helperComponents';
import { observer } from 'mobx-react';

@observer
class InvitePanel extends StoreInputHandler {
  constructor(props) {
    super(props, props.store)
  }

  render() {
    return (
      <Panel bsStyle="warning">
        <Panel.Heading>
          <Panel.Title componentClass="h3">Invite a user</Panel.Title>
        </Panel.Heading>
        <Panel.Body>

          <Form>
            <FormGroup controlId="inviteFormEmail">
              <Col md={10}>
                <FormControl
                  type="email"
                  placeholder="Email of the user you are inviting"
                  name="inviteeEmail"
                  value={this.store.inviteeEmail}
                  onChange={this.handleInputChange}
                />
              </Col>
            </FormGroup>
            <FormGroup>
              <Col md={2}>
                <Button type="button" onClick={this.store.invite}>Invite</Button>
              </Col>
            </FormGroup>
          </Form>

        </Panel.Body>
      </Panel>
    );
  }
}

export default InvitePanel;