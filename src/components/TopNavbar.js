import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Navbar, Nav } from 'react-bootstrap/lib';
import NavItemLink from './NavItemLink';
import { observer, inject } from 'mobx-react';

@inject('identityStore')
@observer
class TopNavbar extends Component {

  store = this.props.identityStore;

  render() {
    return (
      <Navbar staticTop role="navigation">
        <Navbar.Header>
          <Navbar.Brand>
            <Link to="/">Household Budgeter</Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>

          {this.store.isAuthenticated ? (
            <Nav pullRight>
              <NavItemLink to="/households">Households</NavItemLink>
              <NavItemLink to="/invites">Invites</NavItemLink>
              <NavItemLink to="/manage">{this.store.username}</NavItemLink>
              <NavItemLink to="#" onClick={this.store.logout}>Log out</NavItemLink>
            </Nav>
          ) : (
              <Nav pullRight>
                <NavItemLink to="/login">Login</NavItemLink>
                <NavItemLink to="/account/register">Register</NavItemLink>
              </Nav>
            )}
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default withRouter(TopNavbar);
