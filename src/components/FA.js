import React from 'react';

const FA = (props) => {
  const {
    className,
    faStyle,
    ...rest
  } = props;
  const styleStr = faStyle || 's';
  return <i className={`fa${styleStr} fa-${props.name} ${className}`} {...rest}></i>
}

export default FA;