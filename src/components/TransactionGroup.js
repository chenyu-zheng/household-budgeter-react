import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Panel, Table, Button } from 'react-bootstrap/lib';
import { observer } from 'mobx-react';

@observer
class TransactionGroup extends Component {

  formatNumber = (number) => {
    return number.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 });
  }

  renderTdAmount = (item) => {
    const amountStr = this.formatNumber(item.amount);
    return item.isVoid ? <td className="text-muted"><s>{amountStr}</s></td> :
      item.amount >= 0 ? <td className="text-primary">{amountStr}</td> :
        <td className="text-danger">{amountStr}</td>;
  }

  onVoidText = (item) => {
    return `void this transaction [Amount: ${this.formatNumber(item.amount)}, Description: ${item.description}]`;
  }

  render() {
    const { type, data, userId, onToggle, expanded, household, onVoid, onEdit, onDelete, ...rest } = this.props;
    return (
      <Panel id={`${type}-${data.uid}`} onToggle={onToggle} expanded={expanded} {...rest}>
        <Panel.Heading>
          <Panel.Title toggle>
            {`${data.name} (${data.transactionsCount})`}
          </Panel.Title>
        </Panel.Heading>
        <Panel.Collapse>
          <Panel.Body>
            <Table responsive>
              <thead>
                <tr>
                  {/* <th>Uid</th> */}
                  {type !== 'category' && <th>Category</th>}
                  {type !== 'hAccount' && <th>Account</th>}
                  <th>Amount</th>
                  <th>Date</th>
                  <th>Creator</th>
                  <th>Description</th>
                  <th>Status</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {data.transactions.map(item => (
                  <tr key={item.uid}>
                    {/* <th>{item.uid}</th> */}
                    {type !== 'category' && <td>{item.categoryName}</td>}
                    {type !== 'hAccount' && <td>{item.hAccountName}</td>}
                    {this.renderTdAmount(item)}
                    <td>{item.created}</td>
                    <td>{item.creatorFullName}</td>
                    <td>{item.description}</td>
                    {item.isVoid ? <td className="text-danger">Voided</td> : <td className="text-success">Ok</td>}
                    {(household.creator.id === userId || userId === item.creatorId) ?
                      (item.isVoid ||
                        <td style={{textAlign: "right"}}>
                          <Link to={`/households/${household.uid}/transactions/${item.uid}/Edit`}>
                            <Button bsSize="xs" style={{marginRight: '15px'}}>Edit</Button>
                          </Link>
                          <Button bsSize="xs" onClick={() => onVoid(this.onVoidText(item), item.uid)}>
                            Void
                          </Button>
                        </td>
                      ) :
                      <td></td>
                    }
                  </tr>
                ))}
              </tbody>
            </Table>
            <hr />
            {type === "hAccount" ?
              <strong className={data.balance < 0 ? 'text-danger' : ''}>Balance: $ {this.formatNumber(data.balance)}</strong> :
              <strong className={data.total < 0 ? 'text-danger' : ''}>Total: $ {this.formatNumber(data.total)}</strong>
            }
            {household.creator.id === userId  && <Button bsSize="sm" style={{ float: 'right' }} onClick={onDelete}>
              Delete</Button>}
            {household.creator.id === userId  && <Button bsSize="sm" style={{ float: 'right', marginRight: '20px' }} onClick={onEdit}>
              Edit</Button>}
          </Panel.Body>
        </Panel.Collapse>
      </Panel >
    );
  }
}

export default TransactionGroup;