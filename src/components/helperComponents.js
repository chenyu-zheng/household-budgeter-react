import { Component } from 'react';

export class StoreInputHandler extends Component {
  constructor(props, store) {
    if (Object.keys(store).length === 0) {
      throw new Error("Wrong constructor parameter: store");
    }
    super(props);
    this.store = store;
  }

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    this.store[target.name] = value;
  }
}

export class StoreCleaveHelper extends StoreInputHandler {

  handleCleaveChange = (event) => {
    this.store[event.target.name] = event.target.rawValue;
  }

  handleCleaveFocus = (event) => {
    throw new Error('This handler has not been implemented!')
  }
}

