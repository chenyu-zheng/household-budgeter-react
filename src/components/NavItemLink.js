import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class NavItemLink extends Component {

  render() {
    const {
      active,
      activeKey,
      activeHref,
      onSelect,

      children,
      to,

      ...otherProps
    } = this.props;
    return (
      <li role="presentation" {...otherProps}>
        <Link to={to}>{children}</Link>
      </li>
    );
  }
}

export default NavItemLink;