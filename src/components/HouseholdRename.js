import React from 'react';
import { Panel, Form, Button, FormGroup, FormControl, Col } from 'react-bootstrap/lib';
import { observer } from 'mobx-react';
import { StoreInputHandler } from './helperComponents';

@observer
class HouseholdRename extends StoreInputHandler {
  constructor(props) {
    super(props, props.store)
  }
  render() {
    const { newName, rename, isRenameShown, toggleRename } = this.props.store;
    return (
      <Panel id="household-rename" bsStyle="success" expanded={isRenameShown} onToggle={toggleRename}>
        <Panel.Heading>
          <Panel.Title toggle>
            {isRenameShown ? "Hide" : "Change household name"}
          </Panel.Title>
        </Panel.Heading>
        <Panel.Collapse>
          <Panel.Body>
            <Form>
              <FormGroup controlId="householdRenameFromNewName">
                <Col md={8}>
                  <FormControl
                    type="text"
                    placeholder="New name"
                    name="newName"
                    value={newName}
                    onChange={this.handleInputChange}
                  />
                </Col>
              </FormGroup>
              <FormGroup>
                <Col md={2}>
                  <Button type="button" onClick={rename}>Change</Button>
                </Col>
              </FormGroup>
            </Form>
          </Panel.Body>
        </Panel.Collapse>
      </Panel>
    );
  }
}

export default HouseholdRename;