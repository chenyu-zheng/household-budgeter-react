import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class HouseholdItem extends Component {
  render() {
    const { name, membersCount, creator, uid } = this.props.data;
    return (
      <div>
        <h3><Link to={`/households/${uid}`}>{name}</Link></h3>
        <p>Creator: {creator.fullName}</p>
        <p>Members: {membersCount}</p>
        {this.props.userId === creator.id && "Created by me"}
      </div>
    );
  }
}

export default HouseholdItem;