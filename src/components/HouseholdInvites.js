import React, { Component } from 'react';
import { Panel, Well, Button } from 'react-bootstrap/lib';
import { observer } from 'mobx-react';

@observer
class HouseholdInvites extends Component {
  render() {
    const { invites, isInvitesShown, toggleInvites, removeInvite } = this.props.store;
    return (
      <Panel id="household-invites" bsStyle="warning" expanded={isInvitesShown} onToggle={toggleInvites}>
        <Panel.Heading>
          <Panel.Title toggle>
            {isInvitesShown ? "Hide invites" : "View invites"}
          </Panel.Title>
        </Panel.Heading>
        <Panel.Collapse>
          <Panel.Body>
            {invites.map(item => (
              <Well key={item.uid}>
                {item.inviteeEmail}
                <Button bsSize="xs" bsStyle="danger" onClick={() => removeInvite(item.uid)} 
                style={{ float: "right" }}>Remove</Button>
              </Well>
            ))}
          </Panel.Body>
        </Panel.Collapse>
      </Panel>
    );
  }
}

export default HouseholdInvites;