import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import ErrorList from './ErrorList';
import { Alert } from 'react-bootstrap/lib';

const DangerModal = (props) => {
  const { show, onHide, onConfirm, actionText, children, errors, ...rest } = props;
  return (
    <Modal show={show} onHide={onHide} {...rest}>
      <Modal.Body>
        <h3 className="text-danger">Are you sure you want to {actionText || 'do this'}?</h3>
        <ErrorList errors={errors} />
        {children && <Alert bsStyle="warning">{children}</Alert>}
      </Modal.Body>

      <Modal.Footer>
        <Button onClick={onHide}>Cancel</Button>
        <Button bsStyle="danger" onClick={onConfirm}>Confirm</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default DangerModal;
