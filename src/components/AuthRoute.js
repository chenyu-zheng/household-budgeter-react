import React from 'react';
import { Route, Redirect, withRouter } from 'react-router-dom'
import { observer, inject } from 'mobx-react';

const AuthRoute = (props) => {
  const {
    identityStore,
    ...otherProps
  } = props;
  if (!identityStore.isAuthenticated) {
    return <Redirect to='/login' />;
  }
  return (
    <Route {...otherProps}>
    </Route>
  );
};

export default withRouter(inject('identityStore')(observer(AuthRoute)));