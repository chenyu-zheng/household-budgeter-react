import React, { Component } from 'react';
import { Panel, Table, Button } from 'react-bootstrap/lib';
import { observer } from 'mobx-react';
import FA from './FA';

@observer
class MembersPanle extends Component {
  render() {
    if (!this.props.store.household) {
      return (
        <Panel bsStyle="success">
          <Panel.Heading>
            <Panel.Title componentClass="h3">Members</Panel.Title>
          </Panel.Heading>
          <Panel.Body>
            Loading...
        </Panel.Body>
        </Panel>
      );
    } else {
      const { household, members, userId, toggleModal, removeMember } = this.props.store;
      return (
        <Panel bsStyle="success">
          <Panel.Heading>
            <Panel.Title componentClass="h3">Members ({members.length})</Panel.Title>
          </Panel.Heading>
          <Panel.Body>
            <Table striped condensed>
              <thead>
                <tr>
                  <th></th>
                  <th>Full Name</th>
                  <th>Email</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {members.map(item => (
                  <tr key={item.id}>
                    <td>{item.id === household.creator.id && <FA style={{color: '#5cb85c', marginLeft: '10px'}} name="user-tie" />}</td>
                    <td>{item.fullName}</td>
                    <td>{item.email}</td>
                    <td>{userId === household.creator.id && item.id !== userId && (
                      <Button bsStyle="danger" bsSize="xs"
                      onClick={() => toggleModal(`remove member [${item.fullName}]`, () => removeMember(item.id))}
                      >
                        Remove
                      </Button>
                    )}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Panel.Body>
        </Panel>
      );
    }
  }
}

export default MembersPanle;