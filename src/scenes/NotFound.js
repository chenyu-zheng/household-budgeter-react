import React from 'react';
import { Button, Grid, Row, Col } from 'react-bootstrap/lib';
import { Link } from 'react-router-dom';

const NotFound = () => {
  return (
    <Grid>
      <Row>
        <Col md={12}>
          <h1>Oops!</h1>
          <h2>404 Not Found</h2>
          <Link to="/"><Button>Back me home</Button></Link>
        </Col>
      </Row>
    </Grid>
  );
}

export default NotFound;