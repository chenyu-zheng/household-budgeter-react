import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { observer, inject } from 'mobx-react';
import {
  Form,
  FormGroup,
  FormControl,
  Col,
  Button,
  ControlLabel,
  Checkbox,
  Row,
  Grid
} from 'react-bootstrap/lib';
import ErrorList from '../components/ErrorList';
import { StoreInputHandler } from '../components/helperComponents';

@inject('loginViewStore')
@observer
class Login extends StoreInputHandler {

  constructor(props) {
    super(props, props.loginViewStore);
  }
  // store = this.props.loginViewStore;
  componentDidMount = () => {
    if (this.store.isAuthenticated) {
      this.store.logout();
    }
  }

  componentWillUnmount = () => {
    this.store.clear();
  }
  render() {
    if (this.store.isSuccessful) {
      return <Redirect to='/households' />;
    } else {
      return (
        <Grid>
          <Row>
            <Col md={6} mdOffset={2}>

              <h3 style={{ textAlign: 'center' }}>{this.store.isLoading ? 'Loading...' : 'Login your account'}</h3>
              <br />

              <ErrorList errors={this.store.errors} />

              <Form horizontal>
                <FormGroup controlId="loginFormEmail">
                  <Col componentClass={ControlLabel} md={3}>
                    Email
                </Col>
                  <Col md={9}>
                    <FormControl
                      type="email"
                      placeholder="Email"
                      name="email"
                      value={this.store.email}
                      onChange={this.handleInputChange}
                      // suggested by google
                      autoComplete="username"
                    />
                  </Col>
                </FormGroup>

                <FormGroup controlId="loginFormPassword">
                  <Col componentClass={ControlLabel} md={3}>
                    Password
                </Col>
                  <Col md={9}>
                    <FormControl
                      type="password"
                      placeholder="Password"
                      name="password"
                      value={this.store.password}
                      onChange={this.handleInputChange}
                      // suggested by google
                      autoComplete="current-password"
                    />
                  </Col>
                </FormGroup>

                <FormGroup>
                  <Col mdOffset={3} md={9}>
                    <Checkbox name="rememberMe"
                      checked={this.store.rememberMe}
                      onChange={this.handleInputChange}
                    >
                      Remember me
                  </Checkbox>
                  </Col>
                </FormGroup>

                <FormGroup>
                  <Col mdOffset={3} md={9}>
                    <Button type="button" onClick={this.store.login}>Login in</Button>
                  </Col>
                </FormGroup>
              </Form>

              <Row>
                <Col mdOffset={1}>
                  <p>
                    <Link to="/account/register">Register</Link>
                  </p>
                  <p>
                    <Link to="/account/forgotPassword">Forget password</Link>
                  </p>
                  <p>
                    <Link to="/account/resetPassword">Reset password</Link>
                  </p>
                </Col>
              </Row>
            </Col>
          </Row>
        </Grid>
      );
    }
  }
}

export default Login;
