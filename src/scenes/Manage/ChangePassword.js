import React from 'react';
import { Link } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import {
  Form,
  FormGroup,
  FormControl,
  Col,
  Button,
  ControlLabel,
  Row,
  Grid
} from 'react-bootstrap/lib';
import ErrorList from '../../components/ErrorList';
import { StoreInputHandler } from '../../components/helperComponents';

@inject('changePasswordViewStore')
@observer
class ChangePassword extends StoreInputHandler {
  constructor(props) {
    super(props, props.changePasswordViewStore);
  }

  componentWillUnmount = () => {
    this.store.clear();
  }

  render() {
    if (this.store.isSuccessful) {
      return (
        <Grid>
          <Row>
            <Col mdOffset={2} md={6}>
              <p>You password has been changed. Please login with your new password.</p>
              <Link to="/login">Login</Link>
            </Col>
          </Row>
        </Grid>
      );
    } else {
      return (
        <Grid>
          <Row>
            <Col mdOffset={2} md={6}>

              <h3 style={{ textAlign: 'center' }}>Change your password</h3>
              <br />
              <ErrorList errors={this.store.errors} />

              <Form horizontal>
                {/* suggested by google */}
                <input type="text" style={{display: 'none'}}
                  value={this.store.rootStore.identityStore.username}
                  onChange={() => {}}
                  autoComplete="username" />

                <FormGroup controlId="changePassWordFormOld">
                  <Col componentClass={ControlLabel} md={3}>
                    Old Password
                  </Col>
                  <Col md={9}>
                    <FormControl
                      type="password"
                      placeholder="Old Password"
                      name="oldPassword"
                      value={this.store.oldPassword}
                      onChange={this.handleInputChange}
                      autoComplete="current-password"
                    />
                  </Col>
                </FormGroup>

                <FormGroup controlId="changePassWordFormNew">
                  <Col componentClass={ControlLabel} md={3}>
                    New Password
                  </Col>
                  <Col md={9}>
                    <FormControl
                      type="password"
                      placeholder="New Password"
                      name="newPassword"
                      value={this.store.newPassword}
                      onChange={this.handleInputChange}
                      autoComplete="new-password"
                    />
                  </Col>
                </FormGroup>

                <FormGroup controlId="changePassWordFormConfirm">
                  <Col componentClass={ControlLabel} md={3}>
                    Confirm password
                  </Col>
                  <Col md={9}>
                    <FormControl
                      type="password"
                      placeholder="Confirm Password"
                      name="confirmPassword"
                      value={this.store.confirmPassword}
                      onChange={this.handleInputChange}
                      autoComplete="new-password"
                    />
                  </Col>
                </FormGroup>

                <FormGroup>
                  <Col mdOffset={3} md={9}>
                    <Button type="button" onClick={this.store.changePassword}>Submit</Button>
                  </Col>
                </FormGroup>
              </Form>
            </Col>
          </Row>
        </Grid>
      );
    }
  }
}

export default ChangePassword;
