import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Col, Row, Grid } from 'react-bootstrap/lib';
import { inject, observer } from 'mobx-react';

@inject('identityStore')
@observer
class Manage extends Component {
  store = this.props.identityStore;
  render() {
    return (
      <Grid>
        <Row>
          <Col mdOffset={2} md={8}>
            <h3 style={{ textAlign: 'center' }}>Manage your account</h3>
            <br />
            <p>Username: {this.store.username}</p>
            <p>First name: {this.store.firstName}</p>
            <p>Last name: {this.store.lastName}</p>
            <p></p>
            <Link to="/manage/changePassword">ChangePassword</Link>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default Manage;