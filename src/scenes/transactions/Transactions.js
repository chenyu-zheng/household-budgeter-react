import React from 'react';
import { Link } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import { Grid, Row, Col, Button, Tabs, Tab, Modal, Form, FormGroup, FormControl, ControlLabel } from 'react-bootstrap/lib';
import ErrorList from '../../components/ErrorList';
import TransactionGroup from '../../components/TransactionGroup';
import { StoreInputHandler } from '../../components/helperComponents';
import DangerModal from '../../components/DangerModal';

@inject('transactionsViewStore')
@observer
class Transactions extends StoreInputHandler {
  constructor(props) {
    super(props, props.transactionsViewStore);
  }
  // store = this.props.transactionsViewStore;

  componentDidMount = () => {
    this.store.load(this.props.match.params.uid);
  }

  componentWillUnmount = () => {
    this.store.clear();
  }

  render() {
    const { household, hAccounts, categories, userId, selectedTab, toggleTab, errors } = this.props.transactionsViewStore;
    if (!household) {
      return (
        <Grid>
          <Row>
            <Col>
              <h1>Loading...</h1>
            </Col>
          </Row>
        </Grid>
      );
    }
    return (
      <Grid>
        <h1>{household.name} <small style={{ float: 'right' }}>Transactions</small></h1>

        <Link to={`/households/${household.uid}`}>
          <Button bsStyle="default" style={{ float: 'right' }}>Go back</Button>
        </Link>
        <Link to={`/households/${household.uid}/transactions/create`}>
          <Button bsStyle="primary" style={{ float: 'right', marginRight: '20px' }}>New transaction</Button>
        </Link>
        <br />
        <Tabs activeKey={selectedTab} id="transactionListTab" onSelect={toggleTab}>
          <br />

          <Tab eventKey={'hAccount'} title="List by Account">
            {userId === household.creator.id &&
              <Button bsStyle="primary" onClick={() => this.store.toggleFormModal(`New Account`, this.store.createHAccount)}>
                New Account
            </Button>}
            <br />
            <br />
            <ErrorList errors={errors} />
            {hAccounts.map(item => (
              <TransactionGroup key={item.uid}
                type="hAccount"
                data={item}
                userId={userId}
                bsStyle="info"
                household={household}
                expanded={item.isOpen}
                onToggle={() => this.store.toggleGroup('hAccount', item.uid)}
                onVoid={(actionText, transactionUid) => {
                  this.store.toggleDangerModal(actionText, () => this.store.voidTransaction(transactionUid))
                }}
                onEdit={() => this.store.toggleFormModal('Rename Account', () => this.store.renameHAccount(item.uid))}
                onDelete={() => this.store.toggleDangerModal(`delete account [${item.name}] and its transactions`,
                  () => this.store.deleteHAccount(item.uid))}
              />
            ))}

          </Tab>

          <Tab eventKey={'category'} title="List by Category">
            {userId === household.creator.id &&
              <Button bsStyle="warning" onClick={() => this.store.toggleFormModal(`New Category`, this.store.createCategory)}>
                New Category
            </Button>}
            <br />
            <br />
            <ErrorList errors={errors} />
            {categories.map(item => (
              <TransactionGroup key={item.uid}
                type="category"
                data={item}
                userId={userId}
                bsStyle="warning"
                household={household}
                expanded={item.isOpen}
                onToggle={() => this.store.toggleGroup('category', item.uid)}
                onVoid={(actionText, transactionUid) => {
                  this.store.toggleDangerModal(actionText, () => this.store.voidTransaction(transactionUid))
                }}
                onEdit={() => this.store.toggleFormModal('Rename Category', () => this.store.renameCategory(item.uid))}
                onDelete={() => this.store.toggleDangerModal(`delete category [${item.name}]`, () => this.store.deleteCategory(item.uid))}
              />
            ))}
          </Tab>

        </Tabs>

        <Modal show={this.store.showFormModal} onHide={this.store.toggleFormModal}>
          <Modal.Body>
            <h3 style={{ textAlign: 'center' }}>{this.store.formTitle}</h3>
            <br />
            <ErrorList errors={this.store.formErrors} />
            <Form horizontal>
              <FormGroup controlId="hAccountCategoryFormName">
                <Col componentClass={ControlLabel} md={2}>
                  Name
                  </Col>
                <Col md={9}>
                  <FormControl
                    type="text"
                    placeholder="Enter a name"
                    name="name"
                    value={this.store.form.name}
                    onChange={this.handleInputChange}
                  />
                </Col>
              </FormGroup>
            </Form>
          </Modal.Body>

          <Modal.Footer>
            <Button onClick={this.store.toggleFormModal}>Cancel</Button>
            <Button bsStyle="primary" onClick={this.store.formOnSubmit}>Submit</Button>
          </Modal.Footer>
        </Modal>

        <DangerModal
          show={this.store.showDangerModal}
          onHide={this.store.toggleDangerModal}
          actionText={this.store.dangerModalText}
          onConfirm={this.store.dangerModalOnConfirm}
          errors={this.store.dangerModalErrors}
        >
          Warning! This operation cannot be undone.
        </DangerModal>
      </Grid>
    );
  }
}

export default Transactions;