import React from 'react';
import { Link } from 'react-router-dom';
import { Grid, Button, Form, FormGroup, FormControl, ControlLabel, Col, Row, Radio, Alert } from 'react-bootstrap/lib';
import Cleave from 'cleave.js/react';
import { observer, inject } from 'mobx-react';
import ErrorList from '../../components/ErrorList';
import { StoreCleaveHelper } from '../../components/helperComponents';

@inject('createTransactionViewStore')
@observer
class CreateTransaction extends StoreCleaveHelper {
  constructor(props) {
    super(props, props.createTransactionViewStore);
  }

  componentDidMount = () => {
    const hUid = this.props.match.params.uid;
    const tUid = this.props.match.params.tUid;
    if (tUid) {
      this.store.setMode('edit');
      this.store.loadTransaction(tUid);
    } else {
      this.store.setMode('create');
    }
    this.store.load(hUid);
  }
  componentWillUnmount = () => {
    this.store.clear();
  }

  handleAmountInit = (cleave) => {
    this.store.updateCleaveValue = () => cleave.setRawValue(this.store.amount);
  }

  render() {
    const { household, hAccounts, categories, form, isCreationSuccess, isEditSuccess, mode } = this.props.createTransactionViewStore;
    if (!household) {
      return (
        <Grid>
          <h1>Loading...</h1>
          <br />
          <br />
          <hr />
        </Grid>
      );
    }
    return (
      <Grid>
        <h1>{household.name} <small style={{ float: 'right' }}>
          {mode === 'create' ? 'New transaction' : 'Edit transaction'}
        </small></h1>
        <Link to={`/households/${household.uid}/transactions`}>
          <Button bsStyle="default" style={{ float: 'right' }}>Go back</Button>
        </Link>
        <br />
        <br />
        <hr />
        <Row>
          <Col mdOffset={2} md={6}>
            {isCreationSuccess && <Alert bsStyle="success">
              Create transaction succeeded!
            </Alert>}
            {isEditSuccess && <Alert bsStyle="success">
              Edit transaction succeeded!
            </Alert>}
            <ErrorList errors={this.store.errors} />
            <Form horizontal>

              <FormGroup controlId="createTransFormType">
                <Col componentClass={ControlLabel} md={3}>
                  Type
                  </Col>
                <Col md={9}>
                  <Radio name="type" inline
                    value="withdrawal"
                    disabled={mode === 'edit'}
                    checked={form.type === 'withdrawal'}
                    onChange={(this.handleInputChange)}>
                    Withdrawal
                    </Radio>{' '}
                  <Radio name="type" inline
                    value="deposit"
                    disabled={mode === 'edit'}
                    checked={form.type === 'deposit'}
                    onChange={this.handleInputChange}>
                    Deposit
                    </Radio>
                </Col>
              </FormGroup>

              <FormGroup controlId="createTransFormAccount">
                <Col componentClass={ControlLabel} md={3}>
                  {form.type === 'withdrawal' ? 'From' : 'To'} account
                  </Col>
                <Col md={9}>
                  <FormControl componentClass="select" name="hAccountUid"
                    disabled={mode === 'edit'}
                    value={form.hAccountUid}
                    onChange={(this.handleInputChange)}>
                    <option value="">Select an account</option>
                    {hAccounts.map(item =>
                      <option value={item.uid} key={item.uid}>{item.name}</option>)}
                  </FormControl>
                </Col>
              </FormGroup>


              <FormGroup controlId="createTransFormCategory">
                <Col componentClass={ControlLabel} md={3}>
                  Cagetory
                </Col>
                <Col md={9}>
                  <FormControl componentClass="select" name="categoryUid"
                    value={form.categoryUid}
                    onChange={this.handleInputChange}>
                    <option value="">Select a category</option>
                    {categories.map(item =>
                      <option value={item.uid} key={item.uid}>{item.name}</option>)}
                  </FormControl>
                </Col>
              </FormGroup>

              <FormGroup controlId="createTransFormAmount">
                <Col componentClass={ControlLabel} md={3}>
                  Amount
                  </Col>
                <Col md={9}>
                  <Cleave className="form-control"
                    disabled={mode === 'edit'}
                    placeholder="Enter your tansaction amount"
                    options={{
                      numeral: true,
                      numeralThousandsGroupStyle: 'thousand'
                    }}
                    name="amount"
                    onInit={this.handleAmountInit}
                    onChange={this.handleCleaveChange} />
                </Col>
              </FormGroup>

              <FormGroup controlId="createTransFormDesc">
                <Col componentClass={ControlLabel} md={3}>
                  Description
                  </Col>
                <Col md={9}>
                  <FormControl
                    type="text"
                    placeholder="Description"
                    name="description"
                    value={this.store.description}
                    onChange={this.handleInputChange}
                  />
                </Col>
              </FormGroup>

              <FormGroup>
                <Col mdOffset={3} md={9}>
                  {mode === 'create' ?
                    <Button type="button" onClick={this.store.create}>Create</Button> :
                    <Button type="button" onClick={() => this.store.edit(this.props.match.params.tUid)}>Submit</Button>
                  }

                </Col>
              </FormGroup>
            </Form>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default CreateTransaction;