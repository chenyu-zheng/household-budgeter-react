import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Col, Row, Grid, Panel, Well, Button, Alert } from 'react-bootstrap/lib';
import ErrorList from '../../components/ErrorList';

@inject('userInvitesViewStore')
@observer
class UserInvites extends Component {

  store = this.props.userInvitesViewStore;

  componentDidMount = () => {
    this.store.load();
  }

  componentWillUnmount = () => {
    this.store.clear();
  }

  render() {
    const { invites, errors, joinedHousehold } = this.store;
    return (
      <Grid>
        <Row>
          <Col mdOffset={3} md={6}>
            <ErrorList errors={errors} />
            {joinedHousehold && (
              <Alert bsStyle="success">
                You joined household: {joinedHousehold.name}
              </Alert>
            )}
            <Panel bsStyle="warning">
              <Panel.Heading>
                <Panel.Title componentClass="h3">
                  {`You have ${invites.length > 0 ? invites.length : 'no'} invite(s)`}
                </Panel.Title>
              </Panel.Heading>
              <Panel.Body>
                {invites.map(item => (
                  <Well key={item.uid}>
                    <p>{item.householdName}</p>
                    <Button onClick={() => this.store.accept(item.uid)}>Accept</Button>
                  </Well>
                ))}
              </Panel.Body>
            </Panel>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default UserInvites;