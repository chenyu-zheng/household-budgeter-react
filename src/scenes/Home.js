import React, { Component } from 'react';
import { Grid, Jumbotron, Button } from 'react-bootstrap/lib';
import { Link } from 'react-router-dom';

class Home extends Component {
  render() {
    return (
      <Grid>
        <Jumbotron>
          <h1>Hello!</h1>
          <h2>Welcome to Household Budgeter!</h2>
          <p>
            To start using this site, please login or register an account.
          </p>
          <p>
            <Link to="/login">
              <Button bsStyle="success" style={{ width: '180px', marginRight: '20px' }}>Login</Button>
            </Link>
            <Link to="/account/register">
              <Button bsStyle="warning" style={{ width: '180px' }}>Register</Button>
            </Link>
          </p>
        </Jumbotron>
        <p>&copy; {new Date().getFullYear()} Chenyu Zheng</p>
      </Grid>
    );
  }
}

export default Home;