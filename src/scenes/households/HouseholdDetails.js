import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import { Grid, Row, Col, Button, Alert } from 'react-bootstrap/lib';
import ErrorList from '../../components/ErrorList';
import MembersPanle from '../../components/MembersPanel';
import InvitePanel from '../../components/InvitePanel';
import HouseholdInvites from '../../components/HouseholdInvites';
import HouseholdRename from '../../components/HouseholdRename';
import DangerModal from '../../components/DangerModal';

@inject('householdDetailsViewStore')
@observer
class HouseholdDetails extends Component {
  constructor(props) {
    super(props, props.householdDetailsViewStore)
    this.store = props.householdDetailsViewStore;
  }

  componentDidMount = () => {
    this.store.clear();
    this.store.load(this.props.match.params.uid);
  }

  componentWillUnmount = () => {

  }

  render() {
    const { household, inviteErrors, renameErrors, modalErrors, userId, } = this.store;
    if (this.store.isLeft) {
      return (
        <Grid>
          <p>You have left {household.name}.</p>
          <Link to="/households">Ok</Link>
        </Grid>
      );
    }
    if (!household) {
      return (
        <Grid>
          <h1>Loading...</h1>
          <br />
          <br />
          <hr />
          <ErrorList errors={renameErrors} />
        </Grid>
      );
    }
    return (
      <Grid>
        <h1>{household.name} <small style={{ float: 'right' }}>Details</small></h1>
        <Link to={`/households/${household.uid}/transactions`}>
          <Button bsStyle="success" style={{ float: 'right' }}>Transactions</Button>
        </Link>
        <br />
        <br />
        <hr />
        <Row>
          <Col md={6} style={{ marginBottom: '30px' }}>
            <MembersPanle store={this.store} />
            <ErrorList errors={renameErrors} />
            {household.creator.id === userId &&
              <HouseholdRename store={this.store} />}
            {household.creator.id !== userId &&
              <Button onClick={() => this.store.toggleModal(`leave [${household.name}]`, this.store.leave)}>
                Leave Household
            </Button>}
          </Col>
          {household.creator.id === userId &&
            <Col md={6}>
              {this.store.lastInvite &&
                <Alert bsStyle="success">
                  You invited {this.store.lastInvite.inviteeEmail}.
                </Alert>}
              <ErrorList errors={inviteErrors} />
              <InvitePanel store={this.store} />
              <HouseholdInvites store={this.store} />
            </Col>}
        </Row>

        <DangerModal
          show={this.store.isModalShown}
          onHide={this.store.toggleModal}
          actionText={this.store.modalActionText}
          onConfirm={this.store.modalOnConfirm}
          errors={modalErrors}
        />
      </Grid>
    );
  }
}

export default HouseholdDetails;