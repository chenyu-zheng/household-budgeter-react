import React from 'react';
import { Redirect } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import {
  Form,
  FormGroup,
  FormControl,
  Col,
  Button,
  Row,
  Grid,
  Panel
} from 'react-bootstrap/lib';
import ErrorList from '../../components/ErrorList';
import { StoreInputHandler } from '../../components/helperComponents';

@inject('createHouseholdViewStore')
@observer
class CreateHousehold extends StoreInputHandler {
  constructor(props) {
    super(props, props.createHouseholdViewStore)
  }

  componentWillUnmount = () => {
    this.store.clear();
  }

  render() {
    if (this.store.isSuccessful) {
      return <Redirect to={this.props.returnUrl} />;
    } else {
      return (
        <Grid>
          <Row>
            <Col mdOffset={3} md={6}>
              <ErrorList errors={this.store.errors} />
              <Panel bsStyle="success">
                <Panel.Heading>
                  <Panel.Title componentClass="h3">
                    Create a new household
                  </Panel.Title>
                </Panel.Heading>
                <Panel.Body>
                  
                  <Form>
                    <FormGroup controlId="CreateHouseholdFormName">
                      <Col md={9}>
                        <FormControl
                          type="text"
                          placeholder="Household name"
                          name="name"
                          value={this.store.name}
                          onChange={this.handleInputChange}
                        />
                      </Col>
                    </FormGroup>

                    <FormGroup>
                      <Col md={2}>
                        <Button type="button" onClick={this.store.create}>Create</Button>
                      </Col>
                    </FormGroup>
                  </Form>

                </Panel.Body>
              </Panel>
            </Col>
          </Row>
        </Grid>
      );
    }
  }
}

export default CreateHousehold;