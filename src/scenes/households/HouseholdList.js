import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Link } from 'react-router-dom';
import { Button, Grid, Table, Panel } from 'react-bootstrap/lib';
import FA from '../../components/FA';
import DangerModal from '../../components/DangerModal';

@inject('householdListViewStore')
@observer
class HouseholdList extends Component {

  store = this.props.householdListViewStore;

  componentDidMount = () => {
    this.store.load();
  }

  componentWillUnmount = () => {
    this.store.clear();
  }

  render() {
    const { households, userId } = this.store;
    return (
      <Grid>
        <Panel bsStyle="success">
          <Panel.Heading>
            <Panel.Title componentClass="h3">
              {this.store.isLoading ? 'Loading...' :
                `You have ${households.length > 0 ? households.length : 'no'} household(s)`}
              <Link to="/households/create" style={{ float: 'right' }}>
                <FA name="plus" /> Create
                </Link>
            </Panel.Title>
          </Panel.Heading>
          <Panel.Body>
            <Table striped condensed>
              <thead>
                <tr>
                  <th></th>
                  <th>Name</th>
                  <th>Creator</th>
                  <th>Members</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {households.map(item => (
                  <tr key={item.uid}>
                    <td>{item.creator.id === userId && <FA style={{ color: '#5cb85c', marginLeft: '10px' }} name="user-tie" />}</td>
                    <td><Link to={`/households/${item.uid}`}>{item.name}</Link></td>
                    <td>{item.creator.fullName}</td>
                    <td>{item.membersCount}</td>
                    <td>
                      {item.creator.id === userId &&
                        <Button bsSize="xs" onClick={() =>
                          this.store.toggleModal(`delete [${item.name}]`, () => this.store.remove(item.uid))}
                        >
                          Delete
                        </Button>}
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Panel.Body>
        </Panel>

        <DangerModal
          show={this.store.isModalShown}
          onHide={this.store.toggleModal}
          actionText={this.store.modalActionText}
          onConfirm={this.store.modalOnConfirm}
          errors={this.store.modalErrors}
        >
          Warning! This operation cannot be undone.
          All transactions, accounts, categories belong to the household will be permanently deleted.
        </DangerModal>
      </Grid>
    );
  }
}

export default HouseholdList;