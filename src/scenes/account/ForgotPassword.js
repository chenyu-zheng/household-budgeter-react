import React from 'react';
import { Link } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import {
  Form,
  FormGroup,
  FormControl,
  Col,
  Button,
  Row,
  Grid
} from 'react-bootstrap/lib';
import ErrorList from '../../components/ErrorList';
import { StoreInputHandler } from '../../components/helperComponents';

@inject('forgotPasswordViewStore')
@observer
class ForgotPassword extends StoreInputHandler {
  constructor(props) {
    super(props, props.forgotPasswordViewStore);
  }

  componentWillUnmount = () => {
    this.store.clear();
  }

  render() {
    if (this.store.isSuccessful) {
      return (
        <Grid>
          <Row>
            <Col mdOffset={2} md={6}>
              <p>Please check your email for your password reset code.</p>
              <Link to="/account/resetPassword">Reset Password</Link>
            </Col>
          </Row>
        </Grid>
      );
    } else {
      return (
        <Grid>
          <Row>
            <Col mdOffset={2} md={6}>
              <h3 style={{ textAlign: 'center' }}>Please enter your email</h3>
              <br />
              <ErrorList errors={this.store.errors} />
              <Form>
                <FormGroup controlId="forgotPasswordFormEmail">
                  <Col md={10}>
                    <FormControl
                      type="email"
                      placeholder="Email"
                      name="email"
                      value={this.store.email}
                      onChange={this.handleInputChange}
                    />
                  </Col>
                </FormGroup>

                <FormGroup>
                  <Col md={2}>
                    <Button type="button" onClick={this.store.submit}>Submit</Button>
                  </Col>
                </FormGroup>
              </Form>
            </Col>
          </Row>
        </Grid>
      );
    }
  }
}

export default ForgotPassword;
