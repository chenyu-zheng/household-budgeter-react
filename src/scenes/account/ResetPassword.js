import React from 'react';
import { Link } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import {
  Form,
  FormGroup,
  FormControl,
  Col,
  Button,
  ControlLabel,
  Row,
  Grid
} from 'react-bootstrap/lib';
import ErrorList from '../../components/ErrorList';
import { StoreInputHandler } from '../../components/helperComponents';

@inject('resetPasswordViewStore')
@observer
class ResetPassword extends StoreInputHandler {
  constructor(props) {
    super(props, props.resetPasswordViewStore);
  }

  componentWillUnmount = () => {
    this.store.clear();
  }

  render() {
    if (this.store.isSuccessful) {
      return (
        <Grid>
          <Row>
            <Col mdOffset={2} md={6}>
              <p>Your password has been reset. Please <Link to="/login">Login</Link>.</p>
            </Col>
          </Row>
        </Grid>
      );
    } else {
      return (
        <Grid>
          <Row>
            <Col mdOffset={2} md={6}>
              
              <h3 style={{textAlign: 'center'}}>Reset your password</h3>
              <br />
              <ErrorList errors={this.store.errors} />
              <Form horizontal>
                <FormGroup controlId="resetPasswordFormEmail">
                  <Col componentClass={ControlLabel} md={3}>
                    Email
                  </Col>
                  <Col md={9}>
                    <FormControl
                      type="email"
                      placeholder="Email"
                      name="email"
                      value={this.store.email}
                      onChange={this.handleInputChange}
                      autoComplete="username"
                    />
                  </Col>
                </FormGroup>

                <FormGroup controlId="resetPasswordFormPassword">
                  <Col componentClass={ControlLabel} md={3}>
                    Password
                  </Col>
                  <Col md={9}>
                    <FormControl
                      type="password"
                      placeholder="Password"
                      name="password"
                      value={this.store.password}
                      onChange={this.handleInputChange}
                      autoComplete="new-password"
                    />
                  </Col>
                </FormGroup>

                <FormGroup controlId="resetPasswordFormConfirmPassword">
                  <Col componentClass={ControlLabel} md={3}>
                    Confirm password
                  </Col>
                  <Col md={9}>
                    <FormControl
                      type="password"
                      placeholder="Confirm password"
                      name="confirmPassword"
                      value={this.store.confirmPassword}
                      onChange={this.handleInputChange}
                      autoComplete="new-password"
                    />
                  </Col>
                </FormGroup>

                <FormGroup controlId="resetPasswordFormCode">
                  <Col componentClass={ControlLabel} md={3}>
                    Code
                  </Col>
                  <Col md={9}>
                    <FormControl
                      type="text"
                      placeholder="Code"
                      name="code"
                      value={this.store.code}
                      onChange={this.handleInputChange}
                    />
                  </Col>
                </FormGroup>

                <FormGroup>
                  <Col mdOffset={3} md={9}>
                    <Button type="button" onClick={this.store.resetPassword}>Reset</Button>
                  </Col>
                </FormGroup>
              </Form>
            </Col>
          </Row>
        </Grid>
      );
    }
  }
}

export default ResetPassword;
