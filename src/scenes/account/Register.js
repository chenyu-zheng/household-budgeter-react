import React from 'react';
import { Redirect } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import {
  Form,
  FormGroup,
  FormControl,
  Col,
  Button,
  ControlLabel,
  Row,
  Grid
} from 'react-bootstrap/lib';
import ErrorList from '../../components/ErrorList';
import { StoreInputHandler } from '../../components/helperComponents';

@inject('registerViewStore')
@observer
class Register extends StoreInputHandler {
  constructor(props) {
    super(props, props.registerViewStore);
  }

  componentWillUnmount = () => {
    this.store.clear();
  }

  render() {
    if (this.store.isSuccessful) {
      return <Redirect to='/login' />;
    } else {
      return (
        <Grid>
          <Row>
            <Col mdOffset={2} md={6}>

              <h3 style={{textAlign: 'center'}}>Reigster a new account</h3>
              <br />
              <ErrorList errors={this.store.errors} />
              <Form horizontal>
                <FormGroup controlId="registerFormEmail">
                  <Col componentClass={ControlLabel} md={3}>
                    Email
                  </Col>
                  <Col md={9}>
                    <FormControl
                      type="email"
                      placeholder="Email"
                      name="email"
                      value={this.store.email}
                      onChange={this.handleInputChange}
                      autoComplete="username"
                    />
                  </Col>
                </FormGroup>

                <FormGroup controlId="registerFormPassword">
                  <Col componentClass={ControlLabel} md={3}>
                    Password
                  </Col>
                  <Col md={9}>
                    <FormControl
                      type="password"
                      placeholder="Password"
                      name="password"
                      value={this.store.password}
                      onChange={this.handleInputChange}
                      autoComplete="new-password"
                    />
                  </Col>
                </FormGroup>

                <FormGroup controlId="registerFormConfirmPassword">
                  <Col componentClass={ControlLabel} md={3}>
                    Confirm password
                  </Col>
                  <Col md={9}>
                    <FormControl
                      type="password"
                      placeholder="Confirm password"
                      name="confirmPassword"
                      value={this.store.confirmPassword}
                      onChange={this.handleInputChange}
                      autoComplete="new-password"
                    />
                  </Col>
                </FormGroup>

                <FormGroup controlId="registerFormFirstName">
                  <Col componentClass={ControlLabel} md={3}>
                    First name
                  </Col>
                  <Col md={9}>
                    <FormControl
                      type="text"
                      placeholder="First name"
                      name="firstName"
                      value={this.store.firstName}
                      onChange={this.handleInputChange}
                    />
                  </Col>
                </FormGroup>

                <FormGroup controlId="registerFormLastName">
                  <Col componentClass={ControlLabel} md={3}>
                    Last name
                  </Col>
                  <Col md={9}>
                    <FormControl
                      type="text"
                      placeholder="Last name"
                      name="lastName"
                      value={this.store.lastName}
                      onChange={this.handleInputChange}
                    />
                  </Col>
                </FormGroup>

                <FormGroup>
                  <Col mdOffset={3} md={9}>
                    <Button type="button" onClick={this.store.register}>Register</Button>
                  </Col>
                </FormGroup>
              </Form>
            </Col>
          </Row>
        </Grid>
      );
    }
  }
}

export default Register;
