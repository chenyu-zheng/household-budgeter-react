import { computed, observable, action } from 'mobx';
import ViewStoreBase from '../ViewStoreBase';

export default class RegisterViewStore extends ViewStoreBase {
  api;
  @observable form = {
    email: '',
    password: '',
    confirmPassword: '',
    firstName: '',
    lastName: ''
  };
  @observable errors = [];
  @observable isSuccessful = false;

  constructor(rootStore, apiMethod) {
    super(rootStore);
    this.api = apiMethod;
  }

  @computed get email() {
    return this.form.email;
  }
  set email(value) {
    this.form.email = value;
  }
  @computed get password() {
    return this.form.password;
  }
  set password(value) {
    this.form.password = value;
  }
  @computed get confirmPassword() {
    return this.form.confirmPassword;
  }
  set confirmPassword(value) {
    this.form.confirmPassword = value;
  }
  @computed get firstName() {
    return this.form.firstName;
  }
  set firstName(value) {
    this.form.firstName = value;
  }
  @computed get lastName() {
    return this.form.lastName;
  }
  set lastName(value) {
    this.form.lastName = value;
  }

  @action clear = () => {
    this.isSuccessful = false;
    this.form = {
      email: '',
      password: '',
      confirmPassword: '',
      firstName: '',
      lastName: ''
    };
    this.errors = [];
  }

  register = async () => {
    this.loading();
    const response = await this.api(
      this.email,
      this.password,
      this.confirmPassword,
      this.firstName,
      this.lastName
    )
    if (response.ok) {
      this.setState({isSuccessful: true});
    } else {
      this.setState({ errors: response.data });
    }
    this.done();
  }
}