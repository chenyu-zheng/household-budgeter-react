import { computed, observable, action } from 'mobx';
import ViewStoreBase from '../ViewStoreBase';

export default class ForgotPasswordViewStore extends ViewStoreBase {
  api;
  @observable form = {
    email: ''
  };
  @observable errors = [];
  @observable isSuccessful = false;

  constructor(rootStore, apiMethod) {
    super(rootStore);
    this.api = apiMethod;
  }

  @computed get email() {
    return this.form.email;
  }
  set email(value) {
    this.form.email = value;
  }

  @action clear = () => {
    this.isSuccessful = false;
    this.form = {
      email: ''
    };
    this.errors = [];
  }

  submit = async () => {
    this.loading();
    const response = await this.api(
      this.email
    )
    if (response.ok) {
      this.setState({isSuccessful: true});
    } else {
      this.setState({ errors: response.data });
    }
    this.done();
  }
}