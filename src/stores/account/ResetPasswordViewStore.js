import { computed, observable, action } from 'mobx';
import ViewStoreBase from '../ViewStoreBase';

export default class ResetPasswordViewStore extends ViewStoreBase {
  api;
  @observable form = {
    email: '',
    password: '',
    confirmPassword: '',
    code: ''
  };
  @observable errors = [];
  @observable isSuccessful = false;

  constructor(rootStore, apiMethod) {
    super(rootStore);
    this.api = apiMethod;
  }

  @computed get email() {
    return this.form.email;
  }
  set email(value) {
    this.form.email = value;
  }
  @computed get password() {
    return this.form.password;
  }
  set password(value) {
    this.form.password = value;
  }
  @computed get confirmPassword() {
    return this.form.confirmPassword;
  }
  set confirmPassword(value) {
    this.form.confirmPassword = value;
  }
  @computed get code() {
    return this.form.code;
  }
  set code(value) {
    this.form.code = value;
  }

  @action clear = () => {
    this.isSuccessful = false;
    this.form = {
      email: '',
      password: '',
      confirmPassword: '',
      code: ''
    };
    this.errors = [];
  }

  resetPassword = async () => {
    this.loading();
    const response = await this.api(
      this.email,
      this.password,
      this.confirmPassword,
      this.code
    )
    if (response.ok) {
      this.setState({isSuccessful: true});
    } else {
      this.setState({ errors: response.data });
    }
    this.done();
  }
}