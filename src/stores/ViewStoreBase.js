import { computed } from 'mobx';
import StoreBase from './StoreBase';

export default class ViewStoreBase extends StoreBase {

  // constructor(rootStore) {
  //   super(rootStore)
  // }

  @computed get isAuthenticated() {
    return this.rootStore.isAuthenticated;
  }

}