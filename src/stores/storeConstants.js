export const STORE_STATE = {
  DONE: 'done',
  PENDING: 'pending',
  ERROR: 'error'
};