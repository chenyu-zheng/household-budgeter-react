import { observable, computed, action } from 'mobx';
import ViewStoreBase from '../ViewStoreBase';

export default class HouseholdDetailsViewStore extends ViewStoreBase {
  api;
  identityStore;
  @observable household = null;
  @observable members = [];
  @observable isLeft = false;
  @observable errors = [];
  @observable inviteForm = { inviteeEmail: '' };
  @observable inviteErrors = [];
  @observable invites = [];
  @observable isInvitesShown = false;
  @observable lastInvite = null;
  @observable renameForm = { newName: '' };
  @observable renameErrors = [];
  @observable isRenameShown = false;
  @observable isModalShown = false;
  @observable modalErrors = [];
  @observable modalActionText = null;
  @observable modalOnConfirm = null;

  constructor(rootStore, {
    get,
    update,
    listMembers,
    invite,
    leave,
    removeMember,
    listInvites,
    removeInvite
  }) {
    super(rootStore);
    this.identityStore = rootStore.identityStore;
    this.api = {
      get,
      update,
      listMembers,
      invite,
      leave,
      removeMember,
      listInvites,
      removeInvite
    };
  }

  @computed get userId() {
    return this.identityStore.userId;
  }

  @computed get inviteeEmail() {
    return this.inviteForm.inviteeEmail;
  }
  set inviteeEmail(value) {
    this.inviteForm.inviteeEmail = value;
  }
  @computed get newName() {
    return this.renameForm.newName;
  }
  set newName(value) {
    this.renameForm.newName = value;
  }

  @action clearInviteForm = () => {
    this.inviteForm = { inviteeEmail: '' };
  }

  @action clearRenameForm = () => {
    this.renameForm = { newName: '' };
  }

  @action clear = () => {
    this.clearInviteForm();
    this.clearRenameForm();
    this.errors = [];
    this.inviteErrors = [];
    this.household = null;
    this.members = [];
    this.lastInvite = null;
    this.isLeft = false;
    this.invites = [];
    this.isInvitesShown = false;
    this.renameErrors = [];
    this.isRenameShown = false
    this.isModalShown = false;
    this.modalErrors = [];
    this.modalActionText = '';
    this.modalOnConfirm = null;
  }

  load = async (uid) => {
    await this.loadHousehold(uid);
    await this.loadMembers();
  }

  loadHousehold = async (uid) => {
    this.loading();
    const response = await this.api.get(this.identityStore.validToken, uid);
    if (response.ok) {
      this.setState({ household: response.data });
    } else {
      this.setState({ errors: [...this.errors, ...response.data] });
    }
    this.done();
  }

  loadMembers = async () => {
    this.loading();
    const response = await this.api.listMembers(this.identityStore.validToken, this.household.uid);
    if (response.ok) {
      this.setState({ members: response.data });
    } else {
      this.setState({ errors: [...this.errors, ...response.data] });
    }
    this.done();
  }

  loadInvites = async () => {
    this.loading();
    const response = await this.api.listInvites(this.identityStore.validToken, this.household.uid);
    if (response.ok) {
      this.setState({ invites: response.data });
    } else {
      this.setState({ errors: [...this.errors, ...response.data] });
    }
    this.done();
  }

  removeMember = async (memberId) => {
    this.loading();
    const response = await this.api.removeMember(this.identityStore.validToken, this.household.uid, memberId);
    if (response.ok) {
      if (this.isModalShown) {
        this.toggleModal();
      }
      await this.loadMembers();
    } else {
      this.setState({ errors: response.data });
    }
    this.done();
  }

  rename = async () => {
    if (this.newName === this.household.name) {
      return;
    }
    this.loading();
    const response = await this.api.update(this.identityStore.validToken, this.household.uid, this.newName);
    if (response.ok) {
      this.clearRenameForm();
      this.setState({ renameErrors: [] })
      await this.loadHousehold(this.household.uid);
    } else {
      this.setState({ renameErrors: response.data });
    }
    this.done();
  }

  invite = async () => {
    this.loading();
    const response = await this.api.invite(
      this.identityStore.validToken,
      this.household.uid,
      this.inviteeEmail
    );
    this.setState({ lastInvite: null });
    if (response.ok) {
      this.setState({
        lastInvite: response.data,
        inviteErrors: []
      });
      this.clearInviteForm();
      if (this.isInvitesShown) {
        await this.loadInvites();
      }
    } else {
      this.setState({ inviteErrors: response.data });
    }
    this.done();
  }

  removeInvite = async (inviteUid) => {
    this.loading();
    const response = await this.api.removeInvite(this.identityStore.validToken, inviteUid);
    if (response.ok) {
      if (this.isInvitesShown) {
        await this.loadInvites();
      }
    } else {
      this.setState({ errors: response.data });
    }
    this.done();
  }

  leave = async () => {
    this.loading();
    const response = await this.api.leave(this.identityStore.validToken, this.household.uid);
    if (response.ok) {
      if (this.isModalShown) {
        this.toggleModal();
      }
      this.setState({ isLeft: true });
    } else {
      this.setState({ modalErrors: response.data });
    }
    this.done();
  }

  @action toggleInvites = async () => {
    this.isInvitesShown = !this.isInvitesShown;
    if (this.isInvitesShown) {
      await this.loadInvites();
    }
  }

  @action toggleRename = () => {
    this.isRenameShown = !this.isRenameShown;
    if (!this.isInvitesShown) {
      this.renameErrors = [];
    }
  }

  @action toggleModal = (actionText, onConfirm) => {
    this.isModalShown = !this.isModalShown;
    if (this.isModalShown) {
      this.modalActionText = actionText;
      this.modalOnConfirm = onConfirm;
    } else {
      this.modalErrors = [];
      this.modalActionText = '';
      this.modalOnConfirm = null;
    }
  }
}