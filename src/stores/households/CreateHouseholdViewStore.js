import { computed, observable, action } from 'mobx';
import ViewStoreBase from '../ViewStoreBase';

export default class CreateHouseholdViewStore extends ViewStoreBase {
  api;
  @observable form = {
    name: ''
  };
  @observable errors = [];
  @observable isSuccessful = false;

  constructor(rootStore, apiMethod) {
    super(rootStore);
    this.identityStore = rootStore.identityStore;
    this.api = apiMethod;
  }

  @computed get name() {
    return this.form.name;
  }
  set name(value) {
    this.form.name = value;
  }

  @action clear = () => {
    this.isSuccessful = false;
    this.form = {
      name: ''
    };
    this.errors = [];
  }

  create = async () => {
    this.loading();
    const response = await this.api(
      this.identityStore.validToken,
      this.name
    )
    if (response.ok) {
      this.setState({isSuccessful: true});
    } else {
      this.setState({ errors: response.data });
    }
    this.done();
  }
}