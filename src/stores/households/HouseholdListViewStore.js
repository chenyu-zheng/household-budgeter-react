import { observable, computed, action } from 'mobx';
import ViewStoreBase from '../ViewStoreBase';

export default class HouseholdListViewStore extends ViewStoreBase {
  api;
  identityStore;
  @observable households = [];
  @observable errors = [];
  @observable isModalShown = false;
  @observable modalErrors = [];
  @observable modalActionText = '';
  @observable modalOnConfirm = null;

  constructor(rootStore, getApi, removeApi) {
    super(rootStore);
    this.identityStore = rootStore.identityStore;
    this.api = {
      get: getApi,
      remove: removeApi
    }
  }

  @computed get userId() {
    return this.identityStore.userId;
  }

  @action clearModal = () => {
    this.modalErrors = [];
    this.modalActionText = '';
    this.modalOnConfirm = null;
  }

  @action clear = () => {
    this.errors = [];
    this.isModalShown = false;
    this.clearModal();
  }

  load = async () => {
    this.loading();
    const response = await this.api.get(this.identityStore.validToken);
    if (response.ok) {
      this.setState({ households: response.data });
    } else {
      this.setState({ errors: response.data });
    }
    this.done();
  }

  remove = async (uid) => {
    this.loading();
    const response = await this.api.remove(this.identityStore.validToken, uid);
    if (response.ok) {
      this.isModalShown && this.toggleModal();
      this.load();
    } else {
      this.setState({ errors: response.data });
    }
    this.done();
  }

  @action toggleModal = (actionText, onConfirm) => {
    this.isModalShown = !this.isModalShown;
    if (this.isModalShown) {
      this.modalActionText = actionText;
      this.modalOnConfirm = onConfirm;
    } else {
      this.clearModal();
    }
  }
}