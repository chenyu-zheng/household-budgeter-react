import { observable, action } from 'mobx';

export default class StoreBase {
  rootStore;
  @observable isLoading = false;

  constructor(rootStore) {
    this.rootStore = rootStore;
  }
  
  @action loading = () => {
    this.isLoading = true;
  }

  @action done = () => {
    this.isLoading = false;
  }

  @action setState = (state) => {
    Object.keys(state).forEach(key => this[key] = state[key]);
  }

}