import { observable, action, computed } from 'mobx';
import StoreBase from './StoreBase';
import * as LocalStorage from '../API/localStorage';
import * as SessionStorage from '../API/sessionStorage';
import * as TokenApi from '../API/token';

export default class IdentityStore extends StoreBase {
  storage;
  @observable username;
  @observable expires;
  @observable accessToken;
  @observable userId;
  @observable firstName;
  @observable lastName;
  @observable errors = [];

  constructor(rootStore) {
    super(rootStore);
    this.accessToken = LocalStorage.read("access_token");
    this.expires = parseInt(LocalStorage.read("expires"));
    this.username = LocalStorage.read("username");
    this.userId = LocalStorage.read("userId");
    this.firstName = LocalStorage.read("firstName");
    this.lastName = LocalStorage.read("lastName");
    if (this.accessToken && this.expires && this.username) {
      this.storage = LocalStorage;
    } else {
      this.storage = SessionStorage;
    }
  }

  @computed get isTokenExpired() {
    return Date.now() > this.expires;
  }

  @computed get isAuthenticated() {
    return this.username && this.accessToken && !this.isTokenExpired;
  }

  @computed get validToken() {
    if (!this.isAuthenticated) {
      return null;
    }
    return this.accessToken;
  }

  @action login = async (username, password, rememberMe = false) => {
    this.logout();
    this.storage = rememberMe ? LocalStorage : SessionStorage;
    this.loading();
    try {
      const response = await TokenApi.getToken(username, password);
      if (response.ok) {
        this.loadFromJson(response.data);
        this.saveToStorage();
      } else {
        this.setState({
          errors: response.data
        })
      }
      return response;
    } catch (error) {
      console.error(error);
    }
    this.done();
  }

  @action logout = () => {
    this.rootStore.clearAll();
    this.storage.clear();
    this.storage = sessionStorage;
  }

  @action loadFromJson = (json) => {
    this.accessToken = json.access_token;
    this.expires = Date.parse(json[".expires"]);
    this.username = json.userName;
    this.userId = json.userId;
    this.firstName = json.firstName;
    this.lastName = json.lastName;
  }

  @action saveToStorage = () => {
    this.storage.save("access_token", this.accessToken);
    this.storage.save("expires", this.expires.toString());
    this.storage.save("username", this.username);
    this.storage.save("userId", this.userId);
    this.storage.save("firstName", this.firstName);
    this.storage.save("lastName", this.lastName);
  }

  @action loadFromStorage = () => {
    try {
      this.accessToken = this.storage.read("access_token");
      this.expires = parseInt(this.storage.read("expires"));
      this.username = this.storage.read("username");
      this.userId = this.storage.read("userId");
      this.firstName = this.storage.read("firstName");
      this.lastName = this.storage.read("lastName");
    } catch (error) {
      console.error(error);
    }
  }

  @action clear = () => {
    this.username = null;
    this.expires = null;
    this.accessToken = null;
    this.userId = null;
    this.firstName = null;
    this.lastName = null;
    this.errors = [];
  }
}