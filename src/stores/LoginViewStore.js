import { computed, observable, action } from 'mobx';
import ViewStoreBase from './ViewStoreBase';

export default class LoginViewStore extends ViewStoreBase {
  identityStore;
  @observable form = {
    email: '',
    password: '',
    rememberMe: false
  };
  @observable errors = [];
  @observable isSuccessful = false;

  constructor(rootStore) {
    super(rootStore);
    this.identityStore = rootStore.identityStore;
  }

  @computed get email() {
    return this.form.email;
  }
  set email(value) {
    this.form.email = value;
  }
  @computed get password() {
    return this.form.password;
  }
  set password(value) {
    this.form.password = value;
  }
  @computed get rememberMe() {
    return this.form.rememberMe;
  }
  set rememberMe(value) {
    this.form.rememberMe = value === true ? true : false;
  }

  @action clear = () => {
    this.form = {
      email: '',
      password: '',
      rememberMe: false
    };
    this.errors = [];
    this.isSuccessful = false;
  }

  login = async () => {
    this.loading();
    const response = await this.identityStore.login(this.email, this.password, this.rememberMe)
    if (response.ok) {
      this.setState({ isSuccessful: true });
    } else {
      this.setState({ errors: response.data });
    }
    this.done();
  }

  logout = () => {
    this.identityStore.logout();
  }
}