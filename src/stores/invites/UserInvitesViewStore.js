import { observable, action } from 'mobx';
import ViewStoreBase from '../ViewStoreBase';

export default class UserInvitesViewStore extends ViewStoreBase {
  api;
  identityStore;
  @observable invites = [];
  @observable errors = [];
  @observable joinedHousehold = null;

  constructor(rootStore, getApi, acceptApi) {
    super(rootStore);
    this.identityStore = rootStore.identityStore;
    this.api = {
      get: getApi,
      accept: acceptApi
    };
  }

  @action clear = () => {
    this.invites = [];
    this.errors = [];
    this.joinedHousehold = null;
  }

  load = async () => {
    this.loading();
    const response = await this.api.get(this.identityStore.validToken);
    if (response.ok) {
      this.setState({ invites: response.data });
    } else {
      this.setState({ errors: response.data });
    }
    this.done();
  }

  accept = async (uid) => {
    this.loading();
    const response = await this.api.accept(this.identityStore.validToken, uid);
    if (response.ok) {
      this.setState({ 
        joinedHousehold: response.data,
        errors: []
      });
      await this.load();
    } else {
      this.setState({ errors: response.data });
      this.done();
    }
  }
}