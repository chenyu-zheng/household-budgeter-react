import IdentityStore from './IdentityStore';
import { computed } from 'mobx';
import * as AccountApi from '../API/account';
import * as CurrentUserApi from '../API/currentUser';
import * as HouseholdApi from '../API/households';
import * as InviteApi from '../API/invitations';
import * as TransactionApi from '../API/transactions';
import * as HAccountApi from '../API/hAccounts';
import * as CategoryApi from '../API/categories';
import LoginViewStore from './LoginViewStore';
import RegisterViewStore from './account/RegisterViewStore';
import ChangePasswordViewStore from './manage/ChangePasswordViewStore';
import ForgotPasswordViewStore from './account/ForgotPasswordViewStore';
import HouseholdListViewStore from './households/HouseholdListViewStore';
import CreateHouseholdViewStore from './households/CreateHouseholdViewStore';
import ResetPasswordViewStore from './account/ResetPasswordViewStore';
import HouseholdDetailsViewStore from './households/HouseholdDetailsViewStore';
import UserInvitesViewStore from './invites/UserInvitesViewStore';
import TransactionsViewStore from './transactions/TransactionsViewStore';
import CreateTransactionViewStore from './transactions/CreateTransactionViewStore';
import StoreBase from './StoreBase';

export default class RootStore {
  identityStore;
  culture = navigator.language;
  timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;

  constructor() {
    this.identityStore = new IdentityStore(this);
    this.loginViewStore = new LoginViewStore(this);
    this.registerViewStore = new RegisterViewStore(this, AccountApi.register);
    this.changePasswordViewStore = new ChangePasswordViewStore(this, AccountApi.changePassword);
    this.forgotPasswordViewStore = new ForgotPasswordViewStore(this, AccountApi.forgotPassword);
    this.resetPasswordViewStore = new ResetPasswordViewStore(this, AccountApi.resetPassword);
    this.householdListViewStore = new HouseholdListViewStore(this, CurrentUserApi.listHouseholds, HouseholdApi.remove);
    this.createHouseholdViewStore = new CreateHouseholdViewStore(this, HouseholdApi.create);
    this.householdDetailsViewStore = new HouseholdDetailsViewStore(this, {
      get: HouseholdApi.get,
      update: HouseholdApi.update,
      listMembers: HouseholdApi.listMembers,
      removeMember: HouseholdApi.removeMember,
      listInvites: HouseholdApi.listInvitations,
      invite: InviteApi.create,
      removeInvite: InviteApi.remove,
      leave: HouseholdApi.leave
    });
    this.userInvitesViewStore = new UserInvitesViewStore(this, CurrentUserApi.listInvitations, InviteApi.accept);
    this.transactionsViewStore = new TransactionsViewStore(this, {
      listHAccounts: HouseholdApi.listHAccounts,
      listHAccountTransactions: HAccountApi.listTransactions,
      listCategories: HouseholdApi.listCategories,
      listCategoryTransactions: CategoryApi.listTransactions,
      createHAccount: HAccountApi.create,
      createCategory: CategoryApi.create,
      voidTransaction: TransactionApi.revoke,
      renameHAccount: HAccountApi.update,
      renameCategory: CategoryApi.update,
      deleteHAccount: HAccountApi.remove,
      deleteCategory: CategoryApi.remove
    });
    this.createTransactionViewStore = new CreateTransactionViewStore(this, {
      create: TransactionApi.create,
      update: TransactionApi.update,
      get: TransactionApi.get
    });
  }

  parseDateTime = (DateTimeStr) => {
    return new Date(DateTimeStr).toLocaleString(this.culture, { timeZone: this.timeZone });
  }

  @computed get isAuthenticated() {
    return this.identityStore.isAuthenticated;
  }

  clearAll = () => {
    const keys = Object.getOwnPropertyNames(this)
      .filter(key => this[key] instanceof StoreBase);

    keys.forEach(key => {
      try {
        this[key].clear();
      } catch (error) {
        console.error(error);
        console.error(`Failed to run [clear] method on [${key}], probably because you have not implemented this method.`)
      }
    });
  }
}