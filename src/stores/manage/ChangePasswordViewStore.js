import { computed, observable, action } from 'mobx';
import ViewStoreBase from '../ViewStoreBase';

export default class ChangePasswordViewStore extends ViewStoreBase {
  api;
  identityStore;
  @observable form = {
    oldPassword: '',
    newPassword: '',
    confirmPassword: ''
  };
  @observable errors = [];
  @observable isSuccessful = false;

  constructor(rootStore, apiMethod) {
    super(rootStore);
    this.identityStore = rootStore.identityStore;
    this.api = apiMethod;
  }

  @computed get oldPassword() {
    return this.form.oldPassword;
  }
  set oldPassword(value) {
    this.form.oldPassword = value;
  }
  @computed get newPassword() {
    return this.form.newPassword;
  }
  set newPassword(value) {
    this.form.newPassword = value;
  }
  @computed get confirmPassword() {
    return this.form.confirmPassword;
  }
  set confirmPassword(value) {
    this.form.confirmPassword = value;
  }

  @action clear = () => {
    this.isSuccessful = false;
    this.form = {
      oldPassword: '',
      newPassword: '',
      confirmPassword: ''
    };
    this.errors = [];
  }

  changePassword = async () => {
    this.loading();
    const response = await this.api(
      this.identityStore.validToken,
      this.oldPassword,
      this.newPassword,
      this.confirmPassword,
    )
    if (response.ok) {
      this.setState({isSuccessful: true});
    } else {
      this.setState({ errors: response.data });
    }
    this.done();
  }
}