import { observable, computed, action } from 'mobx';
import ViewStoreBase from '../ViewStoreBase';

export default class CreateTransactionViewStore extends ViewStoreBase {
  api;
  @observable mode = 'create';
  @observable form = {
    type: 'withdrawal',
    amount: '',
    description: '',
    hAccountUid: '',
    categoryUid: '',
  };
  @observable errors = []
  @observable isCreationSuccess = false;
  @observable isEditSuccess = false;

  constructor(rootStore, { create, update, get }) {
    super(rootStore);
    this.api = { create, update, get };
  }

  @computed get type() {
    return this.form.type;
  }
  set type(value) {
    if (value !== 'withdrawal' && value !== 'deposit') {
      throw new Error(`Form field "type": the value must be either "withdrawal" or "deposit". The value your are trying to set: "${value}".`);
    }
    this.form.type = value;
  }
  @computed get amount() {
    return this.form.amount;
  }
  set amount(value) {
    this.form.amount = value;
  }
  @computed get description() {
    return this.form.description;
  }
  set description(value) {
    this.form.description = value;
  }
  @computed get hAccountUid() {
    return this.form.hAccountUid;
  }
  set hAccountUid(value) {
    this.form.hAccountUid = value;
  }
  @computed get categoryUid() {
    return this.form.categoryUid;
  }
  set categoryUid(value) {
    this.form.categoryUid = value;
  }

  @computed get userId() {
    return this.rootStore.identityStore.userId;
  }

  @computed get household() {
    return this.rootStore.householdDetailsViewStore.household;
  }

  @computed get hAccounts() {
    return this.rootStore.transactionsViewStore.hAccounts;
  }

  @computed get categories() {
    return this.rootStore.transactionsViewStore.categories;
  }

  @computed get validToken() {
    return this.rootStore.identityStore.validToken;
  }

  @action setMode(value) {
    if (value !== 'create' && value !== 'edit') {
      throw new Error(`The "mode" value must be either "create" or "edit". The value your are trying to set: "${value}".`);
    }
    this.mode = value;
  }

  @action clearForm = () => {
    this.form = {
      type: 'withdrawal',
      amount: '',
      description: '',
      hAccountUid: '',
      categoryUid: '',
    };
  }

  @action clearMessage = () => {
    this.isCreationSuccess = false;
    this.isEditSuccess = false;
    this.errors = [];
  }


  @action clear = () => {
    this.clearForm();
    this.clearMessage();
  }

  load = async (houseUid) => {
    this.loading();
    if (!this.household) {
      await this.rootStore.householdDetailsViewStore.loadHousehold(houseUid);
    }
    if (!this.household) {
      this.setState({ errors: [...this.errors, 'Cannot load household. '] })
      return;
    }
    if (!this.categories.length) {
      await this.rootStore.transactionsViewStore.loadCategories(houseUid);
    }
    if (!this.categories.length) {
      this.setState({ errors: [...this.errors, 'Your household does not have any cagetory. '] })
    }
    if (!this.hAccounts.length) {
      await this.rootStore.transactionsViewStore.loadHAccounts(houseUid);
    }
    if (!this.hAccounts.length) {
      this.setState({ errors: [...this.errors, 'Your household does not have any account. '] })
    }
    this.done();
  }

  _updateCleaveValue = null;
  set updateCleaveValue(callback) {
    this._updateCleaveValue = callback;
  }

  loadTransaction = async (uid) => {
    this.loading();
    const response = await this.api.get(this.validToken, uid);
    if (response.ok) {
      const item = response.data;
      this.setState({
        form: {
          type: item.amount > 0 ? 'deposit' : 'withdrawal',
          amount: String(Math.abs(item.amount)),
          description: item.description,
          hAccountUid: item.hAccountUid,
          categoryUid: item.categoryUid,
        }
      });
      this._updateCleaveValue && this._updateCleaveValue();
    } else {
      this.setState({ errors: response.data });
    }
    this.done();
  }

  create = async () => {
    this.clearMessage();
    this.loading();
    const amount = this.type === 'withdrawal' ? '-' + this.amount : this.amount;
    const response = await this.api.create(this.validToken, amount, this.description, this.household.uid, this.hAccountUid, this.categoryUid);
    if (response.ok) {
      this.setState({
        isCreationSuccess: true
      });
      this.clearForm();
      this._updateCleaveValue && this._updateCleaveValue();
    } else {
      this.setState({ errors: response.data });
    }
    this.done();
  }

  edit = async (uid) => {
    this.clearMessage();
    this.loading();
    const response = await this.api.update(this.validToken, uid, this.description, this.categoryUid);
    if (response.ok) {
      this.setState({
        isEditSuccess: true
      })
    } else {
      this.setState({ errors: response.data });
    }
    this.done();
  }
}