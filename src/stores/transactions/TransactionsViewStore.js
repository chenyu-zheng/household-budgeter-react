import { observable, computed, action, runInAction } from 'mobx';
import ViewStoreBase from '../ViewStoreBase';

export default class TransactionsViewStore extends ViewStoreBase {
  api;
  @observable hAccounts = [];
  @observable categories = [];
  @observable errors = [];
  @observable selectedTab = 'hAccount'; // string: 'hAccount' || 'category'

  @observable showFormModal = false;
  @observable form = {
    name: ''
  }
  @observable formErrors = [];
  @observable formTitle = '';
  @observable formOnSubmit = null;

  @observable showDangerModal = false;
  @observable dangerModalText = null;
  @observable dangerModalOnConfirm = null;
  @observable dangerModalErrors = [];

  constructor(rootStore, {
    listHAccounts,
    listHAccountTransactions,
    createHAccount,
    renameHAccount,
    deleteHAccount,
    listCategories,
    listCategoryTransactions,
    createCategory,
    renameCategory,
    deleteCategory,
    voidTransaction
  }) {
    super(rootStore);
    this.api = {
      listHAccounts,
      listHAccountTransactions,
      createHAccount,
      renameHAccount,
      deleteHAccount,
      listCategories,
      listCategoryTransactions,
      createCategory,
      renameCategory,
      deleteCategory,
      voidTransaction
    };
  }

  @computed get userId() {
    return this.rootStore.identityStore.userId;
  }

  @computed get household() {
    return this.rootStore.householdDetailsViewStore.household;
  }

  @computed get validToken() {
    return this.rootStore.identityStore.validToken;
  }

  @action toggleTab = (value) => {
    if (value !== 'hAccount' && value !== 'category') {
      throw new Error(`The value of "showTab" must be either "hAccount" or "category". Trying to set: "${value}".`);
    }
    this.selectedTab = value;
    if (value === 'hAccount') {
      this.loadHAccounts(this.household.uid);
    } else {
      this.loadCategories(this.household.uid);
    }
  }

  @computed get name() {
    return this.form.name;
  }
  set name(value) {
    if (!this.isLoading) {
      this.form.name = value;
    }
  }

  @action clear = () => {
    this.clearForm();
    this.clearDangerModal();
    this.selectedTab = 'hAccount';
    this.hAccounts = [];
    this.categories = [];
    this.errors = [];
    this.showFormModal = false;
    this.showDangerModal = false;
  }

  @action toggleGroup = (collectionType, uid) => {
    if (collectionType !== 'hAccount' && collectionType !== 'category') {
      throw new Error(`Invalid collectionName: "${collectionType}".`);
    }
    if (collectionType === 'hAccount') {
      const target = this.hAccounts.find(item => item.uid === uid);
      target.isOpen = !target.isOpen;
      target.isOpen && this.loadHAccountTransactions(uid);
    } else {
      const target = this.categories.find(item => item.uid === uid);
      target.isOpen = !target.isOpen;
      target.isOpen && this.loadCategoryTransactions(uid);
    }
  }

  @action toggleFormModal = (title, onSubmit) => {
    this.showFormModal = !this.showFormModal;
    if (this.showFormModal) {
      this.formTitle = title;
      this.formOnSubmit = onSubmit;
    } else {
      this.clearForm();
    }
  }

  @action clearForm = () => {
    this.form = {
      name: ''
    }
    this.formErrors = [];
    this.formTitle = '';
    this.formOnSubmit = null;
  }

  @action toggleDangerModal = (actionText, onConfirm) => {
    this.showDangerModal = !this.showDangerModal;
    if (this.showDangerModal) {
      this.dangerModalText = actionText;
      this.dangerModalOnConfirm = onConfirm;
    } else {
      this.clearDangerModal();
    }
  }

  @action clearDangerModal = () => {
    this.dangerModalText = null;
    this.dangerModalOnConfirm = null;
    this.dangerModalErrors = [];
  }

  parseDateTime = this.rootStore.parseDateTime;

  load = async (houseUid) => {
    this.loading();
    if (!this.household) {
      await this.rootStore.householdDetailsViewStore.loadHousehold(houseUid);
    }
    if (!this.household) {
      this.setState({ errors: [...this.errors, 'Cannot load household. '] })
    } else {
      await this.loadHAccounts(this.household.uid);
      await this.loadCategories(this.household.uid);
    }
    this.done();
  }

  loadHAccounts = async (houseUid) => {
    this.loading();
    const response = await this.api.listHAccounts(this.validToken, houseUid);
    if (response.ok) {
      this.setState({
        hAccounts: response.data.map(item => {
          item['transactions'] = [];
          item['isOpen'] = false;
          return item;
        })
      });
    } else {
      this.setState({ errors: [...this.errors, ...response.data] });
    }
    this.done();
  }

  loadHAccountTransactions = async (hAccountUid) => {
    this.loading();
    const response = await this.api.listHAccountTransactions(this.validToken, hAccountUid);
    if (response.ok) {
      runInAction(() =>
        this.hAccounts.find(a => a.uid === hAccountUid)
          .transactions = response.data.map(t => {
            t.created = this.parseDateTime(t.created);
            return t;
          }));
    } else {
      this.setState({ errors: [...this.errors, ...response.data] });
    }
    this.done();
  }

  loadCategories = async (houseUid) => {
    this.loading();
    const response = await this.api.listCategories(this.validToken, houseUid);
    if (response.ok) {
      this.setState({
        categories: response.data.map(item => {
          item['transactions'] = [];
          item['isOpen'] = false;
          item['total'] = 0;
          return item;
        })
      });
    } else {
      this.setState({ errors: [...this.errors, ...response.data] });
    }
    this.done();
  }

  loadCategoryTransactions = async (categoryUid) => {
    this.loading();
    const response = await this.api.listCategoryTransactions(this.validToken, categoryUid);
    if (response.ok) {
      runInAction(() => {
        const category = this.categories.find(c => c.uid === categoryUid);
        category.transactions = response.data.map(t => {
          t.created = this.parseDateTime(t.created);
          return t;
        });
        category.total = response.data.reduce((a, t) => a + (t.isVoid ? 0 : t.amount), 0);
      });
    } else {
      this.setState({ errors: [...this.errors, ...response.data] });
    }
    this.done();
  }

  createHAccount = async () => {
    this.loading();
    const response = await this.api.createHAccount(this.validToken, this.household.uid, this.form.name);
    if (response.ok) {
      this.showFormModal && this.toggleFormModal();
      await this.loadHAccounts(this.household.uid);
    } else {
      this.setState({ formErrors: response.data });
    }
    this.done();
  }

  renameHAccount = async (hAccountUid) => {
    this.loading();
    const response = await this.api.renameHAccount(this.validToken, hAccountUid, this.form.name);
    if (response.ok) {
      runInAction(() => this.hAccounts
        .find(a => a.uid === hAccountUid).name = this.form.name)
      this.showFormModal && this.toggleFormModal();
    } else {
      this.setState({ formErrors: response.data });
    }
    this.done();
  }

  deleteHAccount = async (hAccountUid) => {
    this.loading();
    const response = await this.api.deleteHAccount(this.validToken, hAccountUid);
    if (response.ok) {
      this.showDangerModal && this.toggleDangerModal();
      await this.loadHAccounts(this.household.uid);
    } else {
      this.setState({ dangerModalErrors: response.data });
    }
    this.done();
  }

  createCategory = async () => {
    this.loading();
    const response = await this.api.createCategory(this.validToken, this.household.uid, this.form.name);
    if (response.ok) {
      this.showFormModal && this.toggleFormModal();
      await this.loadCategories(this.household.uid);
    } else {
      this.setState({ formErrors: response.data });
    }
    this.done();
  }

  renameCategory = async (categoryUid) => {
    this.loading();
    const response = await this.api.renameCategory(this.validToken, categoryUid, this.form.name);
    if (response.ok) {
      runInAction(() => this.categories
        .find(c => c.uid === categoryUid).name = this.form.name)
      this.showFormModal && this.toggleFormModal();
    } else {
      this.setState({ formErrors: response.data });
    }
    this.done();
  }

  deleteCategory = async (categoryUid) => {
    this.loading();
    const response = await this.api.deleteCategory(this.validToken, categoryUid);
    if (response.ok) {
      this.showDangerModal && this.toggleDangerModal();
      await this.loadCategories(this.household.uid);
    } else {
      this.setState({ dangerModalErrors: response.data });
    }
    this.done();
  }

  voidTransaction = async (uid) => {
    this.loading();
    const response = await this.api.voidTransaction(this.validToken, uid);
    if (response.ok) {
      const item = response.data;
      let type, typeUid, prop;
      if (this.selectedTab === 'hAccount') {
        type = 'hAccounts';
        typeUid = item.hAccountUid;
        prop = 'balance';
      } else {
        type = 'categories';
        typeUid = item.categoryUid;
        prop = 'total';
      }
      runInAction(() => {
        const collection = this[type].find(c => c.uid === typeUid);
        collection[prop] -= item.amount;
        collection.transactions.find(t => t.uid === uid).isVoid = true;
      })
      this.showDangerModal && this.toggleDangerModal();
    } else {
      this.setState({ dangerModalErrors: response.data });
    }
    this.done();
  }
}